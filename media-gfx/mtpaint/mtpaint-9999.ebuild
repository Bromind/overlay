# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
EAPI="5"
inherit eutils git-2

DESCRIPTION="Mark Tyler's Painting Program"
SLOT="0"

HOMEPAGE="http://mtpaint.sourceforge.net"
SRC_URI=""
EGIT_REPO_URI="git://github.com/wjaguar/mtPaint.git"
LICENCE="GPL-v3"
KEYWORDS="~amd64"


