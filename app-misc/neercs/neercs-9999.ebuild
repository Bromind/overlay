# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
EAPI="5"
inherit eutils git-2

DESCRIPTION="The high-technology terminal multiplexor"
SLOT="0"

HOMEPAGE="http://caca.zoy.org/wiki/neercs"
SRC_URI=""
EGIT_REPO_URI="git://git.zoy.org/neercs.git"
LICENCE="WTFPL"
KEYWORDS="~amd64"

# pkg_setup() {
# }

src_configure() {
	elog "Begin bootstrap"
	${WORKDIR}/$PF/bootstrap
	elog "end bootstrap, begin configure"
	econf
}

